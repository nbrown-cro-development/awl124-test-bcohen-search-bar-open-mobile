import "./modules/variation_1.scss"
import bc_log_event from "./modules/BcLog"
import ga_send_event from "./modules/Tracking"

const test_id = "awl124"
const variation = "Variation 1"
const custom_dimension = "17"

function init() {
  bc_log_event("init", "Variation Called")
  if (!document.body.classList.contains(`${test_id}_loaded`)) {
    bc_log_event("init", "Body Class Check Passed")
    document.body.classList.add(`${test_id}_loaded`);
    
    ga_send_event(custom_dimension, test_id, variation, "Loaded", true);
    document.querySelector('.gui-search').classList.add('gui-show')

    document.querySelector('.gui-search-toggle').addEventListener('click', _ => {
      let searchOpen = document.querySelector('.gui-search').classList.contains('gui-show') ? 'Opened Search' : "Closed Search";
      ga_send_event(custom_dimension, test_id, 'Search Interaction', searchOpen, false);
    })
  }
}

function conditions() {
  return typeof ga !== "undefined" && typeof document.querySelector('.gui-search') !== null;
}

function pollElements() {
  bc_log_event("pollElements", "Poller Called")
  let x = 0;

  let waitForLoad = function waitForLoad() {
    bc_log_event("pollElements", `Conditions Check: ${conditions()}`)
    if (conditions()) {
      init();
    } else if (!conditions() && x < 10) {
      x++;
      window.setTimeout(waitForLoad, 5);
    }
  };

  window.setTimeout(waitForLoad, 5);
}

pollElements();